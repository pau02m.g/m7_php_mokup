$(function() {
    $("#BotonLogin").click(function () {
        var nombre = $("#correo").val();
        var pass = $("#pass").val();

        console.log(nombre);    // comprobaciones
        console.log(pass);      // comprobaciones

        $.ajax({
            method: "GET",
            url: "PHP/login.php",
            data: {
                "userName": nombre,
                "password": pass
            },

            success: function (data) {
                console.log(data);
                console.log("Peticion correctamente arribada");
                if(data==1){
                    $("#Conexiones").text("Las credenciales son correctas");
                    window.location = "StartMenu.html";
                }
                else{
                    $("#Conexiones").text("Donde vas manito?");
                    window.location = "register.html";
                }
            },
            error: function(jqXHR, texStatus, error) {
                console.log("Error:" +jqXHR.responseText+" "+ texStatus + " " + error);
            }
        });

    });

    $("#BotonSalir").click(function (){
        window.close();
    })

});